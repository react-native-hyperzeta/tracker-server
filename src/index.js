const express = require('express')
const mongoose = require('mongoose')

const app = express()

const mongoDBURI = "mongodb+srv://admin:Administrator@trackercluster.ek662.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
mongoose.connect(mongoDBURI, {
    useNewUrlParser : true,
    useCreateIndex : true,
})

mongoose.connection.on('connected',()=>{
    console.log("connected to MongoDB")
})

mongoose.connection.on('error',()=>{
    console.error("Error connecting to MongoDB")
})

app.get('/', (req, res) => {
    res.send("Hi there !")
})

app.listen(3000, ()=>{
    console.log('Listening on port 3000')
})